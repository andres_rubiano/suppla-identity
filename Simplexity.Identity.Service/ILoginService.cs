﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Simplexity.Identity.Service.App_Data;
using Simplexity.Identity.Service.DTO;

namespace Simplexity.Identity.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ILoginService" in both code and config file together.
    [ServiceContract]
    public interface ILoginService
    {
        [OperationContract]
        MessageDTO Login(string username, string password);
        
        [OperationContract]
        MessageDTO Logout(string username);

    }
}

using System.Runtime.Serialization;

namespace Simplexity.Identity.Service.App_Data
{
    [DataContract]
    public class ResponseDTO
    {
        [DataMember]
        public string IdUsuario { get; set; }
        [DataMember]
        public string CodigoUsuario { get; set; }
        [DataMember]
        public string Nombres { get; set; }
        [DataMember]
        public string CodigoCompania { get; set; }
        [DataMember]
        public string NombreCompania { get; set; }
        [DataMember]
        public string CodigoSubsidiary { get; set; }
        [DataMember]
        public string NombreSubsidiary { get; set; }
        [DataMember]
        public string Email { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Simplexity.Identity.Service.App_Data;

namespace Simplexity.Identity.Service.DTO
{
    public class MessageDTO
    {
       
        public MessagesConstants.Types TypeEnum { set; get; }
        private int type;
        public int Type
        {
            get { return (int)TypeEnum; }
            set { type = value; }
        }
        //
        public string Message { get; set; }
        public int TransactionNumber { get; set; }

        public ResponseDTO Response { get; set; }
    }

    public class MessagesConstants
    {
        public enum Types
        {
            None = 0,
            Information = 1,
            Warning = 2,
            Error = 3
        }
    }
}
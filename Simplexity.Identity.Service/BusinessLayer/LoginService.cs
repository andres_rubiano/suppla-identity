﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using Simplexity.Identity.Service.App_Data;
using Simplexity.Identity.Service.DTO;
using Simplexity.Identity.Service.EF;

namespace Simplexity.Identity.Service.BusinessLayer
{
    public class LoginService
    {

        public ResponseDTO Message { get; set; }

        public MessageDTO Login(string username, string password, string ip)
        {
            // 0. Everything all right so far
            var result = new MessageDTO
            {
                TypeEnum = MessagesConstants.Types.Information
                ,
                TransactionNumber = 0
            };
            using (var entity = new Entities())
            {
                var user = (from u in entity.adUsers
                            join c in entity.adCompanies on u.Usr_ComCode equals c.ComCode
                            join s in entity.adSubsidiaries on u.Usr_SubCode equals s.SubCode
                            where u.UsrCode == username
                            select u).FirstOrDefault();
                // 1. Validate if User Exist
                if (user == null)
                {
                    result.Message = "Usuario y/o Contraseña invalidos";
                    result.TypeEnum = MessagesConstants.Types.Error;
                    result.TransactionNumber = 1;
                    return result;
                }
                // 2. Validate active user
                if (!user.UsrStatus.Equals("T"))
                {
                    UpdateInvalidLoginAttemp(username);
                    result.Message = "El código de username ha sido desactivado";
                    result.TypeEnum = MessagesConstants.Types.Error;
                    result.TransactionNumber = 1;
                    return result;
                }
                //invalid attemps
                if (user.UsrInvalidLogins > 2)
                {
                    BlockUser(username);
                    result.Message = "El código ha sido bloqueado.  Comuníquese con el administrador.";
                    result.TypeEnum = MessagesConstants.Types.Error;
                    result.TransactionNumber = 1;
                    return result;
                }

                if (EsUsuarioInterno(user))
                {
                    // Si no hay mensaje de respuesta:
                    string respuesta = ValidarUsuarioEnDirectorioActivo(user.UsrCode, password);

                    if (!string.IsNullOrEmpty(respuesta))
                    {
                        result.Message = respuesta;
                        result.TypeEnum = MessagesConstants.Types.Error;
                        result.TransactionNumber = 1;
                        return result;
                    }
                }
                else
                {
                    if (CalculateMD5Hash(password) != user.UsrPassword)
                    {
                        UpdateInvalidLoginAttemp(username);
                        result.Message = "Usuario y/o Contraseña invalidos.";
                        result.TypeEnum = MessagesConstants.Types.Error;
                        result.TransactionNumber = 1;
                        return result;
                    }
                }

                // Borra los ingresos inválidos anteriores
                ClearInvalidLoginAttemps(username);
                // Libera el bloqueo al día siguiente
                ClearSessions(username, ip);
                // Adicione una session 
                AddSession(ip, user.Usr_ComCode, user.UsrNick, user.UsrCode, user.Usr_SubCode, user.Usr_LanCode);

                // Solo para usuarios externos:
                if (EsUsuarioIExterno(user))
                    AddLastPassword(username);

                var company = (entity.adCompanies.Where(c => c.ComCode == user.Usr_ComCode)).FirstOrDefault().ComName;
                var subsidiary = (entity.adSubsidiaries.Where(s => s.SubCode == user.Usr_SubCode)).FirstOrDefault().SubName;

                result.Response = new ResponseDTO
                    {
                        CodigoCompania = user.Usr_ComCode,
                        CodigoSubsidiary = user.Usr_SubCode,
                        CodigoUsuario = user.UsrCode,
                        Email = user.UsrEmail,
                        IdUsuario = user.UsrIdentification,
                        NombreCompania = company,
                        Nombres = user.UsrFirstName + " " + user.UsrLastName,
                        NombreSubsidiary = subsidiary
                    };
            }

            return result;
        }

        private string ValidarUsuarioEnDirectorioActivo(string userName, string pass)
        {
            try
            {
                var dominio = ConfigurationManager.AppSettings["DomainActiveDirectory"];
                using (var pc = new PrincipalContext(ContextType.Domain, dominio))
                {
                    bool isValid = pc.ValidateCredentials(userName, pass);
                    if (!isValid)
                    {
                        return "Usuario y/o Contraseña invalidos.";
                    }
                }
            }
            catch (Exception e)
            {
                return e.Message;
            }

            return string.Empty;
        }

        private bool EsUsuarioInterno(adUsers user)
        {
            if (string.IsNullOrEmpty(user.UsrType))
                return false;

            if (user.UsrType.Equals(Constants.UsuarioInterno))
                return true;

            return false;
        }

        private bool EsUsuarioIExterno(adUsers user)
        {
            if (string.IsNullOrEmpty(user.UsrType))
                return true;

            if (user.UsrType.Equals(Constants.UsuarioExterno))
                return true;

            return false;
        }

        private string CalculateMD5Hash(string input)
        {
            var md5 = MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);
            var sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString().ToLower();
        }
        public void ClearInvalidLoginAttemps(string username)
        {
            using (var entity = new Entities())
            {
                var user = entity.adUsers.FirstOrDefault(users => users.UsrCode == username);
                if (user != null)
                {
                    user.UsrInvalidLogins = 0;
                    user.UsrLastInvalidLogin = null;
                }
                entity.SaveChanges();
            }
        }
        public void ClearSessions(string code, string ip)
        {
            using (var entity = new Entities())
            {
                var session = (from s in entity.adSessions
                               where s.Ses_UsrCode == code
                               && s.SesIP.Contains(ip)
                               select s);
                foreach (var adSessionse in session)
                {
                    entity.adSessions.Remove(adSessionse);
                }
                entity.SaveChanges();
            }
        }
        public void AddSession(string ip, string company, string nick, string usrcode, string subsidiary, string language)
        {
            if (string.IsNullOrEmpty(ip))
                ip = "127.0.0.1";
            var code = CalculateMD5Hash(Guid.NewGuid().ToString());
            using (var entity = new Entities())
            {
                entity.adSessions.Add(new adSessions()
                {
                    SesIP = ip,
                    SesCode = code,
                    SesLastActivity = DateTime.Now,
                    SesLogin = DateTime.Now,
                    SesSPDate = DateTime.Now,
                    Ses_ComCode = company,
                    Ses_UsrNick = nick,
                    Ses_UsrCode = usrcode,
                    Ses_SubCode = subsidiary,
                    Ses_LanCode = language,
                    Ses_EnvCode = "",
                    SesSPID = -1
                });
                entity.SaveChanges();
            }
        }
        public void AddLastPassword(string username)
        {
            using (var entity = new Entities())
            {
                var user = entity.adUsers.FirstOrDefault(users => users.UsrCode == username);
                if (user != null)
                {
                    user.UsrLastPassword = DateTime.Now;
                }
                entity.SaveChanges();
            }
        }
        public void UpdateInvalidLoginAttemp(string username)
        {
            using (var entity = new Entities())
            {
                var user = entity.adUsers.FirstOrDefault(users => users.UsrCode == username);
                if (user != null)
                {
                    user.UsrInvalidLogins += 1;
                    user.UsrLastInvalidLogin = DateTime.Now;
                }
                entity.SaveChanges();
            }
        }
        public void BlockUser(string username)
        {
            using (var entity = new Entities())
            {
                var user = entity.adUsers.FirstOrDefault(users => users.UsrCode == username);
                if (user != null)
                {
                    user.UsrStatus = "F";
                    user.UsrLastInvalidLogin = DateTime.Now;
                }
                entity.SaveChanges();
            }
        }
        public MessageDTO Logout(string username, string ip)
        {
            var result = new MessageDTO
            {
                TypeEnum = MessagesConstants.Types.Information
                ,
                TransactionNumber = 0
            };
            try
            {
                ClearSessions(username, ip);
                return result;
            }
            catch (Exception ex)
            {
                result.Message = "El código ha sido bloqueado.  Comuníquese con el administrador.";
                result.TypeEnum = MessagesConstants.Types.Error;
                result.TransactionNumber = 1;
                return result;
            }


        }
    }
}
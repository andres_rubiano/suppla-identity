﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Simplexity.Identity.Service.EF
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class Entities : DbContext
    {
        public Entities()
            : base("name=Entities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<adAccessLog> adAccessLog { get; set; }
        public DbSet<adCompanies> adCompanies { get; set; }
        public DbSet<adSessions> adSessions { get; set; }
        public DbSet<adSubsidiaries> adSubsidiaries { get; set; }
        public DbSet<adUsers> adUsers { get; set; }
        public DbSet<adUsersRole> adUsersRole { get; set; }
        public DbSet<adVisibility> adVisibility { get; set; }
    }
}

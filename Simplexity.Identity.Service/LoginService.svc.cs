﻿using System.ServiceModel;
using System.ServiceModel.Channels;
using Simplexity.Identity.Service.DTO;

namespace Simplexity.Identity.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "LoginService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select LoginService.svc or LoginService.svc.cs at the Solution Explorer and start debugging.
    public class LoginService : ILoginService
    {

        public MessageDTO Login(string username, string password)
        {
            var LoginService = new BusinessLayer.LoginService();
            OperationContext context = OperationContext.Current;
            MessageProperties prop = context.IncomingMessageProperties;
            var endpoint =
                prop[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
            var ip = endpoint.Address;
            return LoginService.Login(username, password,ip);
            
        }

        public MessageDTO Logout(string username)
        {
            OperationContext context = OperationContext.Current;
            MessageProperties prop = context.IncomingMessageProperties;
            var endpoint =
                prop[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
            var ip = endpoint.Address;

            var LoginService = new BusinessLayer.LoginService();
            return LoginService.Logout(username, ip);
        }

    }
}
